In order to run this application you first must have Node and Express installed.

In order to run this application locally, you will first have to run npm install in the root of the project to install dependencies

1. Open the project in vscode or something of the like
2. Open the terminal to the root of the project and run npm install
3. Run "npm Index.js"
4. Navigate to your localhost:3001 in your broswer and the application should appear. (Most up to date version of the public node application can be found at http://adayatmydesk.com)

This application uses a free to use open source license from MIT, so anyone can edit the code if they would like.